package test.java.com.nixsolutions.Mocks;

import main.java.com.nixsolutions.Mocks.Robot;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.*;

import static org.junit.Assert.*;

/**
 * Created by kozlov on 4/27/2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class RobotTest {

    public Robot bender;

    @Before
    public void setUp(){
        bender = new Robot();
    }

    @Test
    public void turnRightWorksCorrect(){
        assertEquals(bender.RIGHT, bender.getDirection());
        bender.turnRight();
        assertEquals(bender.DOWN, bender.getDirection());
        bender.turnRight();
        assertEquals(bender.LEFT, bender.getDirection());
        bender.turnRight();
        assertEquals(bender.UP, bender.getDirection());
        bender.turnRight();
        assertEquals(bender.RIGHT, bender.getDirection());
    }

    @Test
    public void turnLeftWorksCorrect(){
        assertEquals(bender.RIGHT, bender.getDirection());
        bender.turnLeft();
        assertEquals(bender.UP, bender.getDirection());
        bender.turnLeft();
        assertEquals(bender.LEFT, bender.getDirection());
        bender.turnLeft();
        assertEquals(bender.DOWN, bender.getDirection());
        bender.turnLeft();
        assertEquals(bender.RIGHT, bender.getDirection());
    }

    @Test
    public void stepForwardWorksCorrect(){
        assertArrayEquals(new int[]{0, 0}, bender.getCoordinates());
        bender.turnLeft();
        bender.stepForward();
        assertArrayEquals(new int[]{0, 1}, bender.getCoordinates());
        bender.turnLeft();
        bender.stepForward();
        assertArrayEquals(new int[]{-1, 1}, bender.getCoordinates());
        bender.turnLeft();
        bender.stepForward();
        assertArrayEquals(new int[]{-1, 0}, bender.getCoordinates());
        bender.turnLeft();
        bender.stepForward();
        assertArrayEquals(new int[]{0, 0}, bender.getCoordinates());
    }

    @Test
    public void getInputStreamWorksCorrect() throws IOException, ClassNotFoundException {
        bender.turnLeft();
        bender.stepForward();
        bender.turnLeft();
        bender.stepForward();
        bender.turnLeft();
        bender.stepForward();
        bender.turnLeft();
        bender.stepForward();
        ObjectInputStream stream = bender.getInptutStream();
        assertArrayEquals(new int[]{0, 1}, (int[]) stream.readObject());
        assertArrayEquals(new int[]{-1, 1}, (int[]) stream.readObject());
        assertArrayEquals(new int[]{-1, 0}, (int[]) stream.readObject());
        assertArrayEquals(new int[]{0, 0}, (int[]) stream.readObject());
    }
}
