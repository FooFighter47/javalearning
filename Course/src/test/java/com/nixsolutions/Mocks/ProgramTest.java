package test.java.com.nixsolutions.Mocks;

import static org.junit.Assert.*;

import main.java.com.nixsolutions.Mocks.Robot;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.FieldReader;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.*;
import java.nio.CharBuffer;

import static org.mockito.Mockito.*;

/**
 * Created by kozlov on 4/27/2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class ProgramTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void shouldWriteCorrectDataToFile() throws IOException, ClassNotFoundException {
        Robot bender = new Robot();
        File log = folder.newFile("bender.txt");
        if(!log.exists()) {
            log.createNewFile();
        }

        String path = "lffrflfrrfff";
        for (char c : path.toCharArray()) {
            switch (c){
                case 'l':{
                    bender.turnLeft();
                    break;
                }
                case 'r':{
                    bender.turnRight();
                    break;
                }
                case 'f':{
                    bender.stepForward();
                    break;
                }
            }
        }

        FileWriter writer = new FileWriter(log);
        ObjectInputStream input = bender.getInptutStream();
        StringBuilder expected = new StringBuilder();
        while(true) {
            try {
                int[] o = (int[]) input.readObject();
                String s = o[0] + "." + o[1] + "\r\n";
                writer.write(s);
                expected.append(s);
            }
            catch (Exception e){break;}
        }

        writer.flush();
        writer.close();

        FileReader reader = new FileReader(log);
        char[] c = new char[expected.length()];
        reader.read(c);
        assertEquals(expected.toString(), new String(c));
    }

    @Test
    public void shouldSendCorrectCommandsToRobot() throws IOException, ClassNotFoundException {
        Robot bender = mock(Robot.class);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        int[] ar = new int[2];
        ar[0] = 0;
        ar[1] = 1;
        os.writeObject(ar);
        ObjectInputStream stream = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));
        when(bender.getInptutStream()).thenReturn(stream);

        File log = folder.newFile("bender.txt");
        if(!log.exists()) {
            log.createNewFile();
        }

        String path = "lffrflfrrfff";
        for (char c : path.toCharArray()) {
            switch (c){
                case 'l':{
                    bender.turnLeft();
                    break;
                }
                case 'r':{
                    bender.turnRight();
                    break;
                }
                case 'f':{
                    bender.stepForward();
                    break;
                }
            }
        }

        FileWriter writer = new FileWriter(log);
        ObjectInputStream input = bender.getInptutStream();
        StringBuilder expected = new StringBuilder();
        while(true) {
            try {
                int[] o = (int[]) input.readObject();
                String s = o[0] + "." + o[1] + "\r\n";
                writer.write(s);
                expected.append(s);
            }
            catch (Exception e){break;}
        }
        writer.flush();
        writer.close();

        verify(bender, times(2)).turnLeft();
        verify(bender, times(7)).stepForward();
        verify(bender, times(3)).turnRight();
    }
}
