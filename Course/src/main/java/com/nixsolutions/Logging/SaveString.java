package main.java.com.nixsolutions.Logging;

import exception.Save;
import org.apache.logging.log4j.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

/**
 * Created by kozlov on 4/11/2016.
 */
public class SaveString implements Save{
    @Override
    public void save(String s, String s1) {
        Logger logger = LogManager.getLogger("PropertiesConfig");
        logger.info("Copying string to a file.");
        File file = new File(s1);
        logger.trace("File object created.");
        logger.trace("Checking if file exists.");
        if(file.exists()) {
            logger.warn("File exists.");
            try {
                throw new FileAlreadyExistsException(s1);
            }
            catch (FileAlreadyExistsException e){
                logger.error("File exists. Impossible to save.");
                System.out.println(e.toString());
            }
        }
        else {
            logger.trace("File doesn't exist.");
            logger.debug("Trying to save.");
            try {
                FileWriter writer = new FileWriter(s1);
                logger.trace("FileWriter created.");
                writer.write(s);
                logger.debug("File saved.");
                writer.close();
                logger.trace("FileWriter closed.");
                logger.info("String copied successfully.");
            } catch (IOException e) {
                logger.error("Unable to save file.");
                System.out.println(e.toString());
            }
        }
        logger.exit();
    }

    public static void main(String[] args) {
        SaveString s = new SaveString();
        s.save("sasdasda", "C:\\t.txt");
    }
}
