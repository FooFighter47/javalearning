package main.java.com.nixsolutions.Singletone;

import sun.reflect.CallerSensitive;

/**
 * Created by kozlov on 4/25/2016.
 */
public class Consumer {
    @Initialize
    static Single s = Single.getInstance();

    @CallerSensitive
    public static void main(String[] args) {

    }
}
