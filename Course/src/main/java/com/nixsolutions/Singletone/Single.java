package main.java.com.nixsolutions.Singletone;

import java.lang.reflect.Field;

/**
 * Created by kozlov on 4/25/2016.
 */
public class Single {
    private static Single ourInstance = new Single();

    public static Single getInstance() {
        if(callerContainsAnnotatedField()){
            ourInstance = new Single();
        }
        return ourInstance;
    }

    private Single() {
    }

    private static boolean containsAnnotatedField(Class callerClass){
        Field[] declaredFields = callerClass.getFields();
        for (Field f :declaredFields) {
            f.setAccessible(true);
            if (f.getType().equals(Single.class) && f.isAnnotationPresent(Initialize.class)){
                return true;
            }
        }
        return false;
    }

    private static boolean callerContainsAnnotatedField(){
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        int stackIndex;
        for (stackIndex = 0; stackIndex < stackTrace.length; stackIndex++) {
            if(stackTrace[stackIndex].getMethodName().equals("getInstance")&&stackTrace[stackIndex].getClassName().equals(Single.class.getName())){
                try {
                    Class<?> c = Class.forName(stackTrace[stackIndex+1].getClassName());
                    if(containsAnnotatedField(c)){
                        return true;
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
