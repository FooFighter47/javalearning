package main.java.com.nixsolutions;

import java.util.Date;
import java.util.Random;

/**
 * Created by kozlov on 4/6/2016.
 */
public class Arrays {

    public static void main(String[] args) {
        int[][] results = new int[2][20];
        for (int i = 0; i < 20; i++) {
            int[] array = new int[10000];
            Random r = new Random();
            for (int j = 0; j < 10000; j++) {
                array[j] = r.nextInt();
            }
            Date d = new Date();
            sort(array);
            long passed = new Date().getTime() - d.getTime();
            System.out.println("Bubble sorting time: " + passed);
            results[0][i] = (int)passed;
            d = new Date();
            java.util.Arrays.sort(array);
            passed = new Date().getTime() - d.getTime();
            System.out.println("Default sorting time: " + passed);
            results[1][i] = (int)passed;
        }
        double result = 0;
        for (int i : results[0]) {
            result += i;
        }
        result = result/20d;
        System.out.println("Middle bubble sorting time: " + result);
        result = 0;
        for (int i : results[1]) {
            result += i;
        }
        result = result/20d;
        System.out.println("Middle default sorting time: " + result);
    }

    public static int[] sort(int[] array){
        int[] result = array.clone();
        for (int i = 0; i < result.length - 1; i++) {
            for (int j = 0; j < result.length - i -1; j++) {
                if(result[j]>result[j+1]){
                    int temp = result[j+1];
                    result[j+1] = result[j];
                    result[j] = temp;
                }
            }
        }
        return result;
    }
}
