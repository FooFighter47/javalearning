package main.java.com.nixsolutions.Generics;

/**
 * Created by kozlov on 4/20/2016.
 */
public interface Converter<I , O> {
    O convert(I from);
}
