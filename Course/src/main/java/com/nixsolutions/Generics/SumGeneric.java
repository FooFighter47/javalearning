package main.java.com.nixsolutions.Generics;

import java.util.*;

/**
 * Created by kozlov on 4/20/2016.
 */
public class SumGeneric {
    public HashMap<String, Double> sumValues(Map<String, List<? extends Number>> map){
        HashMap<String, Double> result = new HashMap<>();
        for (String key : map.keySet()) {
            double sum = 0d;
            for (Number n : map.get(key)) {
                sum += n.doubleValue();
            }
            result.put(key, sum);
        }
        return result;
    }

    public static void main(String[] args) {
        HashMap<String, List<? extends Number>> map = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            List<Integer> iList = new ArrayList<Integer>();
            for (int j = i; j < 10; j++) {
                iList.add(j);
            }
            map.put(String.valueOf(i), iList);
        }

        SumGeneric s = new SumGeneric();

        HashMap<String, Double> dMap = s.sumValues(map);
        for (String key : map.keySet()) {
            System.out.println(key + ": " + map.get(key));
        }
    }
}
