package main.java.com.nixsolutions.Generics;

/**
 * Created by kozlov on 4/20/2016.
 */
public class FloatToDoubleConverter implements Converter<Float, Double> {
    @Override
    public Double convert(Float from) {
        return from.doubleValue();
    }

    public static void main(String[] args) {
        FloatToDoubleConverter converter = new FloatToDoubleConverter();
        Float f = 14.88f;
        Double d = converter.convert(f);
        System.out.println(d.getClass().getName() + ": " + d);
    }
}
