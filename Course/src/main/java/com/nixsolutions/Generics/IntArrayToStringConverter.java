package main.java.com.nixsolutions.Generics;

/**
 * Created by kozlov on 4/20/2016.
 */
public class IntArrayToStringConverter implements Converter<int[], String> {
    @Override
    public String convert(int[] from) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < from.length; i++) {
            builder.append(i);
            if(i!=from.length-1)
                builder.append(" ");
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        IntArrayToStringConverter converter = new IntArrayToStringConverter();
        System.out.println(converter.convert(array));
    }
}
