package main.java.com.nixsolutions.Mocks;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import com.sun.xml.internal.ws.util.ByteArrayBuffer;

import java.io.*;

/**
 * Created by kozlov on 4/27/2016.
 */
public class Robot {

    public final int RIGHT = 0;
    public final int UP = 1;
    public final int LEFT = 2;
    public final int DOWN = 3;

    private int[] coordinates = new int[2];

    private byte direction = 0;

    private ByteArrayOutputStream bos;
    private ObjectOutputStream outStream;

    public Robot() {
        coordinates[0] = 0;
        coordinates[1] = 0;

        try {
            bos = new ByteArrayOutputStream();
            outStream = new ObjectOutputStream(bos);
        }
        catch (Exception e){}
    }

    public void turnLeft(){
        if(direction==3)
            direction = 0;
        else
            direction++;
    }

    public void turnRight(){
        if(direction==0)
            direction = 3;
        else
            direction--;
    }

    public void stepForward() {
        switch (direction){
            case RIGHT:{
                coordinates[0]++;
                break;
            }
            case LEFT:{
                coordinates[0]--;
                break;
            }
            case UP:{
                coordinates[1]++;
                break;
            }
            case DOWN:{
                coordinates[1]--;
                break;
            }
            default:{
                System.out.println("Reflection is not a toy, kids!");
                break;
            }
        }
        try {
            outStream.writeObject(coordinates.clone());
        }
        catch (Exception e){
            String s = e.toString();
        }
    }

    public ObjectInputStream getInptutStream(){
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            ObjectInputStream inStream = new ObjectInputStream(bis);
            return inStream;
        }
        catch (Exception e){ return null;}
    }

    public byte getDirection(){return direction;}

    public int[] getCoordinates(){return coordinates;}
}
