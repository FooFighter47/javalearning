package main.java.com.nixsolutions.Mocks;

import java.io.*;

/**
 * Created by kozlov on 4/27/2016.
 */
public class Program {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Robot bender = new Robot();
        File log = new File("C:/robotlog/bender.txt");
        if(!log.exists()) {
            log.createNewFile();
        }

        String path = "lffrflfrrfff";
        for (char c : path.toCharArray()) {
            switch (c){
                case 'l':{
                    bender.turnLeft();
                    break;
                }
                case 'r':{
                    bender.turnRight();
                    break;
                }
                case 'f':{
                    bender.stepForward();
                    break;
                }
            }
        }

        FileWriter writer = new FileWriter(log);
        ObjectInputStream input = bender.getInptutStream();
        while(true) {
            try {
                int[] o = (int[]) input.readObject();
                String s = o[0] + "." + o[1] + "\r\n";
                writer.write(s);
            }
            catch (Exception e){break;}
        }

        writer.flush();
        writer.close();
    }
}
