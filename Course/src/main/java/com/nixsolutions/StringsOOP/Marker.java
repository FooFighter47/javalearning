package main.java.com.nixsolutions.StringsOOP;

/**
 * Created by kozlov on 4/8/2016.
 */
public class Marker extends writingInstrument {

    @Override
    protected float calculateInkUsed(String s, boolean use){
        int length = s.length();
        int written = builder.length();

        for (char c:s.toCharArray()) {
            if(Character.isSpaceChar(c))
                length--;
        }

        float used = 0;

        if(written>40)
            used+=length*1.21;
        else if(written>20&&written+length>40){
            used+=(40-written)*1.09;
            used+=(written+length-40)*1.21;
        }
        else if(written>20){
            used+=length*1.09;
        }
        else if(written<20&&written+length>20&&written+length>40){
            used+=(20-written);
            used+=20*1.09;
            used+=(written+length-40)*1.21;
        }
        else if(written<20&&written+length>20){
            used+=(20-written);
            used+=(written+length-20)*1.09;
        }
        else
            used+=length;

        if(use){
            inkAmount-=used;
        }

        return used;
    }
}
