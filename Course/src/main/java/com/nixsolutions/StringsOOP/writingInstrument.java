package main.java.com.nixsolutions.StringsOOP;

/**
 * Created by kozlov on 4/8/2016.
 */
public abstract class writingInstrument {
    protected boolean isErasable;
    protected float inkAmount = 100.0f;
    protected StringBuilder builder = new StringBuilder();

    public boolean isErasable(){
        return isErasable;
    }

    public StringBuilder write(StringBuilder s){
        if(inkAmount-calculateInkUsed(s.toString(), false)>0) {
            builder.append(s);
            calculateInkUsed(s.toString(), true);
            //System.out.println(builder);
        }
        else
            System.out.println("Sorry, out of ink.");
        return builder;
    }

    public StringBuilder erase(StringBuilder s){
        if(isErasable&&builder.toString().endsWith(s.toString())){
            builder.delete( builder.length() - s.length(), builder.length());
            //System.out.println(builder);
        }
        else
            System.out.println("Sorry, this can not be erased.");
        return builder;
    }

    protected float calculateInkUsed(String s, boolean use){
        return 0;
    }

    public float getRemainingInk(){
        return inkAmount;
    }

    public StringBuilder getBuilder(){ return builder; }
}
