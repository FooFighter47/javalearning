package main.java.com.nixsolutions.StringsOOP;

/**
 * Created by kozlov on 4/8/2016.
 */
public class Pen extends writingInstrument{

    public Pen(){
        isErasable = false;
    }

    @Override
    protected float calculateInkUsed(String s, boolean use){
        int length = s.length();

        for (char c:s.toCharArray()) {
            if(Character.isSpaceChar(c))
                length--;
        }

        if(use)
            inkAmount-= length*1.15f;
        return length*1.15f;
    }
}
