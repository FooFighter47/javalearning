package main.java.com.nixsolutions.StringsOOP;

import java.util.Random;

/**
 * Created by kozlov on 4/8/2016.
 */
public class Task {
    public static void main(String[] args) {
        Random r = new Random();
        writingInstrument[] instruments = new writingInstrument[10];

        for (int i = 0; i < instruments.length; i++) {
            int j = r.nextInt(3);
            switch (j){
                case 0: {
                    instruments[i] = new Pen();
                    break;
                }

                case 1: {
                    instruments[i] = new Marker();
                    break;
                }

                case 2: {
                    instruments[i] = new Pencil();
                    break;
                }
            }
        }

        for (int i = 0; i < 10; i++) {
            StringBuilder s = new StringBuilder(String.valueOf(r.nextInt(10000)));
            for (int j = 0; j < instruments.length; j++) {
                instruments[j].write(s);
                if(instruments[j].isErasable()){
                    StringBuilder temp = new StringBuilder(s);
                    instruments[j].erase(temp.delete(0, temp.length() - 1));
                }
                System.out.println(instruments[j].getBuilder());
            }
        }
        for (int i = 0; i < instruments.length; i++) {
            for (int j = i + 1; j < instruments.length; j++) {
                if(instruments[j].inkAmount>instruments[i].inkAmount){
                    writingInstrument temp = instruments[i];
                    instruments[i] = instruments[j];
                    instruments[j] = temp;
                }
            }
        }
        for (int i = 0; i < instruments.length; i++) {
            System.out.println(instruments[i].toString() + ": " + instruments[i].getRemainingInk());
        }
    }
}
