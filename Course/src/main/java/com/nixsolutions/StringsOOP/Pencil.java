package main.java.com.nixsolutions.StringsOOP;

/**
 * Created by kozlov on 4/8/2016.
 */
public class Pencil extends writingInstrument{

    private int sharpens = 0;

    public Pencil(){
        isErasable = true;
    }

    @Override
    protected float calculateInkUsed(String s, boolean use){
        int length = s.length();
        int written = builder.length();

        for (char c:s.toCharArray()) {
            if(Character.isSpaceChar(c))
                length--;
        }

        float used = 0;
        int sharpensNeeded = 0;
        used+=length*0.95;
        while ((written+length)/20>sharpens+sharpensNeeded){
            used+=3;
            sharpensNeeded++;
        }
        if(use){
            inkAmount-=used;
            sharpens+=sharpensNeeded;
        }
        return used;
    }
}
