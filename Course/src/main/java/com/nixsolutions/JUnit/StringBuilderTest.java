package main.java.com.nixsolutions.JUnit;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by kozlov on 4/22/2016.
 */
public class StringBuilderTest {
    StringBuilder builder;

    @Before
    public void builderInitiation(){
        builder = new StringBuilder();
    }

    @Test
    public void shouldBeInitiatedWithSetCapacity(){
        builder = new StringBuilder(10);
        assertEquals(10, builder.capacity());
    }

    @Test
    public void shouldBeInitiatedWithSetString(){
        String s = "DatString";
        builder = new StringBuilder(s);
        assertEquals(s, builder.toString());
    }

    @Test
    public void lengthShouldBeEqualToStringStored(){
        String s = "DatString";
        builder.append(s);
        assertEquals(s.length(), builder.length());
    }

    @Test
    public void capacityShouldBeLessOrEqualToLength(){
        String s = "DatString";
        builder.append(s);
        assertTrue(builder.capacity()>=builder.length());
    }

    @Test
    public void appendingWorksCorrectForString(){
        String one = "one";
        String two = "two";
        builder.append(one);
        builder.append(two);
        assertEquals(one+two, builder.toString());
    }

    @Test
    public void appendingWorksCorrectForDouble(){
        double one = 1;
        double two = 2;
        builder.append(one);
        builder.append(two);
        assertEquals(String.valueOf(one)+two, builder.toString());
    }

    @Test
    public void appendingWorksCorrectForBoolean(){
        boolean one = true;
        boolean two = false;
        builder.append(one);
        builder.append(two);
        assertEquals(String.valueOf(one)+two, builder.toString());
    }
    @Test
    public void ineestionWorksCorrectForBoolean(){
        boolean one = true;
        boolean two = false;
        builder.insert(0, one);
        builder.insert(0, two);
        assertEquals(String.valueOf(two)+one, builder.toString());
    }

    @Test
    public void inserionWorksCorrectForString(){
        String one = "one";
        String two = "two";
        builder.insert(0, one);
        builder.insert(0, two);
        assertEquals(two+one, builder.toString());
    }

    @Test
    public void inserionWorksCorrectForDouble(){
        double one = 1;
        double two = 2;
        builder.insert(0, one);
        builder.insert(0, two);
        assertEquals(String.valueOf(two)+one, builder.toString());
    }

    @Test
    public void substringWorksCorrect(){
        String one = "one";
        String two = "two";
        builder.append(one+two);
        assertEquals(two, builder.substring(one.length()));
    }

    @Test
    public void deleteWorksCorrect(){
        String one = "one";
        String two = "two";
        builder.append(one+two);
        builder.delete(0, one.length());
        assertEquals(two, builder.toString());
    }

    @Test
    public void trimToSizeWorksCorrect(){
        builder = new StringBuilder(20);
        String one = "one";
        builder.append(one);
        builder.trimToSize();
        assertEquals(one.length(), builder.capacity());
    }
}
