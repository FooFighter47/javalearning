package main.java.com.nixsolutions.Collections;

import java.util.*;

/**
 * Created by kozlov on 4/11/2016.
 */
public class MyCollection<T> implements Collection<T> {

    private Object[] array;
    private int size = 0;

    public MyCollection(){
        array = new Object[10];
    }

    public MyCollection(int length){
        array = new Object[length];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size==0?true:false;
    }

    @Override
    public boolean contains(Object o) {
        for(int i = 0; i < size; i++){
            if (array[i].equals(o))
                return true;
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new MyIterator();
    }

    @Override
    public Object[] toArray() {
        return array.clone();
    }

    @Override
    public boolean add(T o) {
        if(size + 1>array.length)
            extend();
        array[size] = o;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if(array[i].equals(o)){
                for (int j = i; j < size; j++) {
                    array[j] = array[j + 1];
                }
                size--;
                array[size] = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        boolean changed = false;
        for (Object o : c) {
            try{
                if(add((T)o))
                    changed = true;
            }
            catch (Exception e){}
        }
        return changed;
    }

    @Override
    public void clear() {
        array = new Object[10];
        size = 0;
    }

    @Override
    public boolean retainAll(Collection c) {
        boolean match = false;
        for (int i = 0; i < size; i++) {
            if(!c.contains(array[i])) {
                if(remove(array[i])) {
                    match = true;
                    i--;
                }
            }
        }
        return match;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean match = false;
        for (Object o : c) {
            if(this.contains(o)) {
                if(this.remove(o))
                    match = true;
            }
        }
        return match;
    }

    @Override
    public boolean containsAll(Collection c) {
        for (Object o : c) {
            if(!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if(a.length>=size){
            System.arraycopy(array, 0, a, 0, size);
            return a;
        }
        System.arraycopy(array, 0, a, 0, size);
        return a;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            builder.append(array[i] + " ");
        }
        return builder.toString();
    }

    private void extend(){
        Object[] replace = new Object[(int)(array.length*1.5)];
        for (int i = 0; i < array.length; i++) {
            replace[i] = array[i];
        }
        array = replace;
    }

    public class MyIterator implements Iterator<T>{

        int current = 0;

        @Override
        public boolean hasNext() {
            return ++current<size;
        }

        @Override
        public T next() {
            current+=1;
            return (T)array[current];
        }
    }

    public static void main(String[] args) {
        List toAdd = new ArrayList<Integer>();
        toAdd.add(10);
        toAdd.add(1488);
        toAdd.add(1489);

        MyCollection<Integer> myCollection = new MyCollection<>();

        myCollection.add(13);
        System.out.println("Added 13.");
        System.out.println(myCollection.toString());

        myCollection.addAll(toAdd);
        System.out.println("Added 10, 1488, 1489 bu addAll.");
        System.out.println(myCollection.toString());

        myCollection.retainAll(toAdd);
        System.out.println("Removed 13 by retainAll.");
        System.out.println(myCollection.toString());

        System.out.println("Check if contains 10.");
        System.out.println(myCollection.contains(10));

        System.out.println("Check if contains 10, 1488, 1489 by containsAll.");
        System.out.println(myCollection.containsAll(toAdd));

        myCollection.remove(10);
        System.out.println("Removed 10.");
        System.out.println(myCollection.toString());

        myCollection.removeAll(toAdd);
        System.out.println("Removed 1488, 1489 by removeAll.");
        System.out.println(myCollection.toString());

        System.out.println("Check that no elements left.");
        System.out.println(myCollection.isEmpty());

        myCollection = new MyCollection<Integer>();
        myCollection.add(1);
        myCollection.add(2);
        myCollection.add(3);
        myCollection.add(4);
        myCollection.add(5);

        List additional = new ArrayList<Integer>();
        additional.add(1);
        additional.add(2);
        additional.add(3);

        myCollection.retainAll(additional);
        System.out.println(myCollection.toString());
    }
}
