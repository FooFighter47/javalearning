package main.java.com.nixsolutions.Exceptions;

import exception.Save;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

/**
 * Created by kozlov on 4/11/2016.
 */
public class SaveString implements Save{
    @Override
    public void save(String s, String s1) {
        File file = new File(s1);
        if(file.exists()) {
            try {
                throw new FileAlreadyExistsException(s1);
            }
            catch (FileAlreadyExistsException e){
                System.out.println(e.toString());
            }
        }
        else {
            try {
                FileWriter writer = new FileWriter(s1);
                writer.write(s);
                writer.close();
            } catch (IOException e) {
                System.out.println(e.toString());
            }
        }
    }

    public static void main(String[] args) {
        SaveString s = new SaveString();
        s.save("sasdasda", "C:\\t.txt");
    }
}
