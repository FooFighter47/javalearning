package main.java.com.nixsolutions;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by kozlov on 4/7/2016.
 */
public class Date {
    public static void main(String[] args) {
        //printMonthLengths(2015);
        //printMondays(2016, 10);
        //System.out.println(isFridayThirteen(2018,4,13));
        //printTimePassed(2007, 02, 28);
        printDifferentLocales();
    }

    public static void printMonthLengths(int year){
        Calendar c = Calendar.getInstance();
        c.setLenient(true);
        c.set(Calendar.YEAR, year);
        for (int i = 0; i < 12; i++) {
            c.set(Calendar.MONTH, i);
            System.out.println(c.get(Calendar.MONTH) + 1 + ": " + c.getActualMaximum(Calendar.DAY_OF_MONTH));
        }
    }

    public static void printMondays(int year, int month){
        if(month>12||month<1)
            throw new IllegalArgumentException();
        Calendar c = Calendar.getInstance();
        c.clear();
        //c.setLenient(false);
        c.set(year, month, 1);
        c.add(Calendar.MONTH, -1);
        System.out.println("Mondays for " + month + "." + year);
        int monthLength = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 1; i <= monthLength; i++){
            c.set(Calendar.DAY_OF_MONTH, i);
            if(c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
                System.out.println(i);
        }
    }

    public static boolean isFridayThirteen(int year, int month, int day){
        if(month>12||month<1||day> 31||day<0)
            throw new IllegalArgumentException();
        if(day!=13)
            return false;
        Calendar c = Calendar.getInstance();
        c.setLenient(true);
        c.set(year, month - 1, day);
        if(c.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
            return true;
        return false;
    }

    public static void printTimePassed(int year, int month, int day){
        if(month>12||month<1||day>31||day<0)
            throw new IllegalArgumentException();
        Calendar then = Calendar.getInstance();
        then.set(year, month, day);
        Calendar now = Calendar.getInstance();
        if(now.after(then)) {
            int years = now.get(Calendar.YEAR) - then.get(Calendar.YEAR);
            int months = 0;
            if(now.get(Calendar.MONTH) >= then.get(Calendar.MONTH)) {
                months = now.get(Calendar.MONTH) - then.get(Calendar.MONTH);
            }
            else{
                years--;
                months = now.get(Calendar.MONTH) + 12 - then.get(Calendar.MONTH);
            }
            int days = 0;
            if(now.get(Calendar.DATE) >= then.get(Calendar.DATE)){
                days = now.get(Calendar.DATE) - then.get(Calendar.DATE);
                months++;
            }
            else{
                int i = then.getMaximum(Calendar.DAY_OF_MONTH);
                days = now.get(Calendar.DATE) + now.getMaximum(Calendar.DAY_OF_MONTH) - then.get(Calendar.DATE);
            }
            System.out.println(years + " года, " + months + " месяц и " + days + " дня");
        }
    }

    public static void printDifferentLocales(){
        //Canada
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("en","CA"));
        String s = df.format(new java.util.Date());
        System.out.println(s);
        //Germany
        df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("de","DE"));
        s = df.format(new java.util.Date());
        System.out.println(s);
        //Pakistan
        df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("en","PK"));
        s = df.format(new java.util.Date());
        System.out.println(s);
        //Vietnam
        df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("vi","VN"));
        s = df.format(new java.util.Date());
        System.out.println(s);
    }
}
