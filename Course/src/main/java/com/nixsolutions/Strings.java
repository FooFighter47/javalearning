package main.java.com.nixsolutions;

public class Strings {

    public static void main(String[] args) {
	// write your code here
        System.out.println(isAnagram("asdfsfdsdvfb", "asdfsfdsdvbf"));
    }

    public static void concat(){
        //option1
        String s1 = "Java" + " " + "forever";

        //option2
        String s2 = "Java";
        String s3 = "forever";
        s1 = s2 + " " + s3;

        //option3
        StringBuffer b = new StringBuffer();
        b.append(s2);
        b.append(" ");
        b.append(s3);
        s1 = b.toString();

        //option4
        s1 = s2 + " " + "forever";
    }

    public static String getSign(String s){
        StringBuffer result = new StringBuffer();
        for (char c : s.toCharArray()) {
            if(Character.isUpperCase(c)){
                result.append(c);
            }
        }
        return result.toString();
    }

    public static boolean isAnagram(String a, String b){
        if(a.length()!=b.length())
            return false;
        char[] aArray = a.toCharArray();
        char[] bArray = b.toCharArray();
        for (int i = 0; i < a.length(); i++) {
            boolean found = false;
            for (int j = i; j < a.length(); j++) {
                if(aArray[i] == bArray[j]){
                    bArray[j] = bArray[i];
                    bArray[i] = aArray[i];
                    found = true;
                    break;
                }
            }
            if(!found)
                return false;
        }
        return true;
    }
}
