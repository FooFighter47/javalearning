package main.java.com.nixsolutions.Reflection;

import java.lang.reflect.Field;

public class AnnotatedValues {
    @Public
    int annotatedDefaultValue = 0;
    @Public
    public int annotatedPublicValue = 1;
    @Public
    protected int annotatedProtectedValue = 2;
    @Public
    private int annotatedPrivateValue = 3;

    int defaultValue = 0;
    public int publicValue = 1;
    protected int protectedValue = 2;
    private int privateValue = 3;

    public static void main(String[] args) {
        AnnotatedValuesGetter getter = new AnnotatedValuesGetter();
        AnnotatedValues values = new AnnotatedValues();
        System.out.println("Using java.lang.reflect:");
        for (Field f : values.getClass().getDeclaredFields()) {
            try{
                f.setAccessible(true);
                Object o = getter.getPublicValue(values, f.getName());
                System.out.println(f.getName() + ": " + o);
            }
            catch (Exception e){
                System.out.println(e + " for " + f.getName());
            }
        }
        System.out.println("Using org.reflections.ReflectionUtils:");
        for (Field f : values.getClass().getDeclaredFields()) {
            try{
                f.setAccessible(true);
                Object o = getter.anotherGetPublicValue(values, f.getName());
                System.out.println(f.getName() + ": " + o);
            }
            catch (Exception e){
                System.out.println(e + " for " + f.getName());
            }
        }
    }
}
