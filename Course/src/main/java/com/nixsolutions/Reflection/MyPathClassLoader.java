package main.java.com.nixsolutions.Reflection;

import java.io.*;
import java.lang.ClassLoader;

/**
 * Created by kozlov on 4/20/2016.
 */
public class MyPathClassLoader extends ClassLoader implements PathClassLoader  {

    String dir;
    String loadingClass;

    public MyPathClassLoader(String loadingClass){
        this.loadingClass = loadingClass;
    }

    @Override
    public void setPath(String dir) {
        this.dir = dir;
    }

    @Override
    public String getPath() {
        return dir;
    }

    public Class getFromPath() throws IOException {
        try {
            FileInputStream input = new FileInputStream(new File(dir));
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int data = input.read();

            while (data != -1) {
                buffer.write(data);
                data = input.read();
            }

            input.close();
            byte[] classData = buffer.toByteArray();

            return defineClass(loadingClass, classData, 0, classData.length);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        MyPathClassLoader loader = new MyPathClassLoader("main.java.com.nixsolutions.Reflection.AnnotatedValues");
        loader.setPath("out\\production\\Course\\com\\nixsolutions\\Reflection\\AnnotatedValues.class");
        Class c = loader.getFromPath();
        System.out.println(c.getName());
    }
}
