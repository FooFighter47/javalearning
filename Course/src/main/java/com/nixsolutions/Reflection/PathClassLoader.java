package main.java.com.nixsolutions.Reflection;

/**
 * Created by kozlov on 4/20/2016.
 */
public interface PathClassLoader {
    void setPath(String dir);
    String getPath();
}
