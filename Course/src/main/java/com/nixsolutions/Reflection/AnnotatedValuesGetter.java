package main.java.com.nixsolutions.Reflection;

import static org.reflections.ReflectionUtils.*;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

/**
 * Created by kozlov on 4/19/2016.
 */
public class AnnotatedValuesGetter {
    public Object getPublicValue(Object classObject, String fieldName) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        Class<?> c = classObject.getClass();
        Field f = c.getDeclaredField(fieldName);
        f.setAccessible(true);
        if(f.isAnnotationPresent(Public.class)){
            return f.get(classObject);
        }
        else{
            throw new IllegalAccessException();
        }
    }

    public Object anotherGetPublicValue(Object classObject, String fieldName) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Set<Field> fields = getFields(classObject.getClass(), withAnnotation(Public.class));
        for (Field f : fields) {
            if(f.getName().equals(fieldName)){
                f.setAccessible(true);
                return f.get(classObject);
            }
        }
        throw new IllegalAccessException();
    }
}
