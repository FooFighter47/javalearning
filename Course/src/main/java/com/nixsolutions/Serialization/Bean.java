package main.java.com.nixsolutions.Serialization;

import java.io.Serializable;

/**
 * Created by kozlov on 4/15/2016.
 */
public class Bean implements Serializable {

    private int number = 0;
    private int notForUsers = 10;
    private static final boolean alwaysTrue = true;

    public static boolean isAlwaysTrue(){return alwaysTrue;}

    public void setNumber(int number){this.number = number;}
    public int getNumber(){return number;}

    private void setNotForUsers(int notForUsers){this.notForUsers = notForUsers;}
    private int getNotForUsers(){return notForUsers;}

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Bean other = (Bean) obj;
        if (number != other.number)
            return false;
        if (notForUsers != other.notForUsers)
            return false;
        return true;
    }

    @Override
    public int hashCode(){
        final int num = 42;
        return (number*notForUsers + notForUsers);
    }
}
