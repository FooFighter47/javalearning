package main.java.com.nixsolutions.Serialization;

import java.io.*;

/**
 * Created by kozlov on 4/15/2016.
 */
public class BeanSerializer<T> {
    public static void main(String[] args) {
        Bean one = new Bean();
        one.setNumber(200);
        try{
            BeanSerializer serializer = new BeanSerializer<Bean>();
            Bean two = (Bean) serializer.deserialize(serializer.serialize(one));
            System.out.println((one==two)? "Same object. Imposibru!":"Different objects. Everything's right!");
            System.out.println(one.equals(two)? "They're equal.":"They're different. Something went wrong...");
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    public byte[] serialize(T b) throws IOException {
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        ObjectOutputStream stream = new ObjectOutputStream(bas);
        stream.writeObject(b);
        return bas.toByteArray();
    }

    public T deserialize(byte[] source) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bas = new ByteArrayInputStream(source);
        ObjectInputStream stream = new ObjectInputStream(bas);
        T result = (T) stream.readObject();
        return result;
    }
}
