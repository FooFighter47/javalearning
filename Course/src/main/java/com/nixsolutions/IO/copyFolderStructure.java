package main.java.com.nixsolutions.IO;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.commons.io.FileUtils;

/**
 * Created by kozlov on 4/14/2016.
 */
public class copyFolderStructure {

    public static void main(String[] args) {
        try {
            //System.out.println(copyViaIO("C:\\from", "C:\\to"));
            //System.out.println(copyViaNIO("C:\\from", "C:\\to"));
            System.out.println(copyViaApache("C:\\from", "C:\\to"));
        }
        catch (IOException e){
            System.out.println(e.toString());
        }
    }

    public static boolean copyViaIO(String from, String to) throws IOException {
        File input = new File(from);
        File output = new File(to);

        if(input.exists()&&input.isDirectory()) {
            if(!output.exists())
                output.mkdir();
            File[] files = input.listFiles();
            for (File f : files) {
                File copy = new File(output, f.getName());
                if(f.isDirectory()){
                    copy.mkdir();
                    copyViaIO(f.getAbsolutePath(), copy.getAbsolutePath());
                }
                else if(f.isFile()){
                    FileInputStream in = new FileInputStream(f);
                    FileOutputStream out = new FileOutputStream(copy);
                    BufferedInputStream b = new BufferedInputStream(in);
                    int lengthStream;
                    byte[] buffer = new byte[1024];
                    while((lengthStream = in.read(buffer)) > 0){
                        out.write(buffer, 0, lengthStream);
                    }
                    in.close();
                    out.close();
                }
            }
            return true;
        }
        return false;
    }

    public static boolean copyViaNIO(String from, String to) throws IOException {
        Path input = Paths.get(from);

        SimpleFileVisitor v = new MyFileVisitor(to);

        if(Files.exists(input)&&Files.isDirectory(input)){
            Files.walkFileTree(input, v);
            return true;
        }
        else return false;
    }

    public static boolean copyViaApache(String from, String to) throws IOException {
        File input = new File(from);
        File output = new File(to);

        if(input.exists()&&input.isDirectory()) {
            FileUtils.copyDirectory(input, output);
            return true;
        }
        return false;
    }

    private static class MyFileVisitor extends SimpleFileVisitor<Path>{

        String input;
        String output;

        public MyFileVisitor(String output){this.output = output;}

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
            Files.copy(file, Paths.get(output, file.toString().replace(input, "")));
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path directory, BasicFileAttributes attributes) throws IOException {

            if(input == null){
                Files.createDirectory(Paths.get(output));
                input = directory.toString();
            }
            else{
                Path out =  Paths.get(output, directory.toString().replace(input, ""));
                Files.copy(directory, out);
            }

            return FileVisitResult.CONTINUE;
        }
    }

}
