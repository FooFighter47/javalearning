package main.java.com.nixsolutions.Threads;

import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by kozlov on 4/18/2016.
 */
public class QueueConsumer extends Thread {

    String name;
    boolean even;
    boolean keepReading = true;

    BlockingQueue<Integer> queue;

    public void stopReading(){
        keepReading = false;
        synchronized (queue) {
            queue.notify();
        }
    }

    @Override
    public void run(){
        synchronized (queue) {
            while(keepReading) {
                try {
                    queue.wait();
                }
                catch (InterruptedException e) {
                    System.out.println("InterruptedException перехвачено");
                }
                if (queue.size() > 0 && queue.element() % 2 == 0 && even) {
                    System.out.println("Thread " + name + " took " + queue.remove() + " from query.");
                } else if (queue.size() > 0 && queue.element() % 2 != 0 && !even) {
                    System.out.println("Thread " + name + " took " + queue.remove() + " from query.");
                }
            }
        }
    }

    public QueueConsumer(String name, boolean even, BlockingQueue<Integer> queue){
        this.name = name;
        this.even = even;
        this.queue = queue;
    }

    public static void main(String[] args) throws InterruptedException {
        ArrayBlockingQueue<Integer> q = new ArrayBlockingQueue<Integer>(100);
        Random r = new Random();
        QueueConsumer evens = new QueueConsumer("Evens", true, q);
        QueueConsumer notEvens = new QueueConsumer("NotEvens", false, q);
        evens.start();
        notEvens.start();
        for (int i = 0; i < 100; i++) {
            synchronized (q) {
                q.add(r.nextInt());
                q.notifyAll();
            }
            Thread.sleep(5);
        }

        evens.stopReading();
        notEvens.stopReading();
        evens.join();
        notEvens.join();
    }
}
