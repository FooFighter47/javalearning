package main.java.com.nixsolutions.Threads;

/**
 * Created by kozlov on 4/18/2016.
 */
public class ThreeThreads extends Thread {

    private int counter = 0;
    private boolean keepCounting = true;

    String name;


    public ThreeThreads(String name){
        this.name = name;
    }

    public void stopCounting(){
        keepCounting = false;
    }

    @Override
    public void run(){
        while (keepCounting) {
            try {
                Thread.sleep(1000);
                counter++;
                System.out.println(name + " tread running for " + counter + " seconds now.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {

        ThreeThreads first = new ThreeThreads("First");
        ThreeThreads second = new ThreeThreads("Second");
        ThreeThreads third = new ThreeThreads("Third");

        for (int i = 0; i < 1000; i++) {
            Thread.sleep(10);
            if(i==100)
                first.start();
            if(i==300)
                second.start();
            if(i==500)
                third.start();
        }

        first.stopCounting();
        second.stopCounting();
        third.stopCounting();
        first.join();
        second.join();
        third.join();
    }
}
